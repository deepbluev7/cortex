use std::collections::HashMap;

use serde_json;
use serde_json::Value;

use cortex::replication::edu_event::EduEvent;

pub struct Transaction {
    pub transaction_id: String,
    pub origin: String,
    pub destination: String,
    pub origin_server_ts: u128,
    pub previous_ids: Vec<String>,

    pub edus: Vec<EduEvent>,
    pub v1_pdus: Vec<String>,
    pub v2_pdus: Vec<String>,
}

pub struct PduEventV1 {
    pub event_id: String,
    pub room_id: String,
    pub sender: String,
    pub origin: String,
    pub origin_server_ts: u128,
    // This is "type"
    pub name: String,
    pub state_key: String,
    pub content: Value,
    pub prev_events: Value,
    pub depth: u128,
    pub auth_events: Value,
    pub redacts: String,
    pub unsigned: Value,
    pub hashes: Value,
    pub signatures: HashMap<String, HashMap<String, String>>,
    pub prev_state: Value,
}

pub struct PduEventV2 {
    pub room_id: String,
    pub sender: String,
    pub origin: String,
    pub origin_server_ts: u128,
    // This is "type"
    pub name: String,
    pub state_key: String,
    pub redacts: String,
    pub unsigned: Value,
    pub prev_events: Value,
    pub auth_events: Value,
    pub depth: u128,
    pub hashes: Value,
    pub signatures: HashMap<String, HashMap<String, String>>,
    pub content: Value,
    pub prev_state: Value,
}

pub struct PduEventHash {
    pub sha256: String,
}

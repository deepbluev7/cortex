use cortex::matrix::resolver::HostResolver;

use crate::signing_key::SigningKey;
use crate::transaction::Transaction;
use http::Uri;
use serde_json::{json, Value};
use std::collections::HashMap;
use std::str::FromStr;

pub struct FederationClient {
    origin: String,
    key: SigningKey,
    resolver: HostResolver,

    http_client: reqwest::Client,
}

impl FederationClient {
    pub fn new(server_name: String, key: SigningKey, unsafe_tls: bool) -> Self {
        let http_client = reqwest::ClientBuilder::new()
            .danger_accept_invalid_certs(unsafe_tls)
            .build()
            .unwrap();

        FederationClient {
            origin: server_name,
            key,
            resolver: HostResolver::new(8448),
            http_client,
        }
    }

    pub fn send_transaction(&mut self, transaction: Transaction) {
        let record = self.resolver.host_record(transaction.destination);

        let url = Uri::builder()
            .scheme("https")
            .path_and_query(
                format!(
                    "/_matrix/federation/v1/send/{}/",
                    transaction.transaction_id
                )
                .as_str(),
            )
            .build()
            .unwrap();

        println!("transaction PUT url: {}", url);

        //        self.http_client.put(url).header("Host", record.host.clone())
    }

    fn sign_request(
        &mut self,
        method: String,
        url: http::Uri,
        destination: String,
        body: Value,
    ) -> String {
        let mut sig_body = json!({
            self.origin.clone(): {
            format!("{}:{}", self.key.key_type, self.key.name): self.key.public_key
            }
        });

        let mut signing_body: Value = json!({
        "destination": destination,
        "method": method,
        "origin": self.origin.clone(),
        "uri": url.path_and_query().unwrap().to_string(),
        });

        if !body.is_null() {
            signing_body
                .as_object_mut()
                .unwrap()
                .insert("content".to_string(), body);
        }

        let sig = self.key.sign(signing_body.clone());

        signing_body.as_object_mut().unwrap().insert(
            "signatures".to_string(),
            json!({
            self.origin.clone(): {
            format!("{}:{}", self.key.key_type, self.key.name): self.key.public_key,
            }
            }),
        );

        format!(
            "origin={},key=\"{}:{}\",sig=\"{}\"",
            self.origin, self.key.key_type, self.key.name, sig
        )
    }
}

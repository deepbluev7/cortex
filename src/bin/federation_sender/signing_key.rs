use std::fs::File;
use std::io::Read;

use serde_json::{Map, Value};
use sodiumoxide::crypto::sign;

pub struct SigningKey {
    pub name: String,
    pub key_type: String,

    pub public_key: sign::PublicKey,
    private_key: sign::SecretKey,
}

impl SigningKey {
    pub fn from_raw(content: String) -> SigningKey {
        let mut iter = content.splitn(3, char::is_whitespace);
        let key_type: String = iter.next().unwrap().to_string();
        let name: String = iter.next().unwrap().to_string();
        let unpadded_seed: &str = iter.next().unwrap().trim();

        let seed = sign::Seed::from_slice(&base64::decode(unpadded_seed).unwrap()).unwrap();

        let (public_key, private_key) = sign::keypair_from_seed(&seed);

        SigningKey {
            name,
            key_type,
            public_key,
            private_key,
        }
    }

    pub fn from_file(path: &str) -> SigningKey {
        let mut file = File::open(path).unwrap();
        let mut data = String::new();
        file.read_to_string(&mut data).unwrap();

        Self::from_raw(data)
    }

    pub fn sign(&self, value: Value) -> String {
        let sig = sign::sign_detached(value.to_string().as_bytes(), &self.private_key);
        base64::encode_config(&sig, base64::STANDARD_NO_PAD)
    }
}

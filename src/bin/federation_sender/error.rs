use http::status::StatusCode;
use serde_json::Value;
use std::error;
use std::fmt;

#[derive(Debug)]
pub enum FederationError {
    Connection(reqwest::Error),
    Transaction(TransactionFailureError),
}

impl error::Error for FederationError {
    fn description(&self) -> &str {
        match *self {
            FederationError::Connection(ref e) => "connection error",
            FederationError::Transaction(ref e) => "transaction failure",
        }
    }

    fn cause(&self) -> Option<&error::Error> {
        match *self {
            FederationError::Connection(_) => None,
            FederationError::Transaction(_) => None,
        }
    }
}

impl fmt::Display for FederationError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            FederationError::Connection(ref e) => write!(f, "reqwest error"),
            // This is a wrapper, so defer to the underlying types' implementation of `fmt`.
            FederationError::Transaction(ref e) => write!(f, "transaction error"),
        }
    }
}

#[derive(Debug)]
pub struct TransactionFailureError {
    pub backoff_for: u128,
    pub code: StatusCode,
    pub error: String,
    pub error_code: String,
    pub host: String,
}

impl TransactionFailureError {
    pub fn new(host: String, code: StatusCode, resp: Value) -> TransactionFailureError {
        TransactionFailureError {
            host,
            code,
            backoff_for: resp.get("retry_after_ms").unwrap().as_u64().unwrap_or(0) as u128,
            error: resp
                .get("error")
                .unwrap()
                .as_str()
                .unwrap_or("")
                .to_string(),
            error_code: resp
                .get("errcode")
                .unwrap()
                .as_str()
                .unwrap_or("")
                .to_string(),
        }
    }
}

impl fmt::Display for TransactionFailureError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.error)
    }
}

impl From<TransactionFailureError> for FederationError {
    fn from(err: TransactionFailureError) -> FederationError {
        FederationError::Transaction(err)
    }
}

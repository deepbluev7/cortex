use crate::error::FederationError;
use std::collections::HashMap;
use std::time::Duration;
use std::time::SystemTime;

pub struct SBackoff {
    pub delay_for: Duration,
    pub ts: u128,
    pub is_down: bool,
}

pub struct Backoff {
    host: HashMap<String, SBackoff>,
}

impl Backoff {
    const MAX_DELAY: Duration = Duration::from_secs(86400);
    const HTTP_BACKOFF: Duration = Duration::from_secs(900);
    const BACKOFF: Duration = Duration::from_secs(30);

    pub fn new() -> Self {
        Backoff {
            host: HashMap::new(),
        }
    }

    pub fn clear_backoff(&mut self, host: String) {
        self.host.remove(host.as_str());
    }

    pub fn host_is_down(&self, host: String) -> bool {
        if let Some(h) = self.host.get(host.as_str()) {
            if h.is_down {
                return SystemTime::now()
                    .duration_since(SystemTime::UNIX_EPOCH)
                    .unwrap()
                    .as_millis()
                    < (h.ts + h.delay_for.as_millis());
            }
        }

        false
    }

    pub fn host_backoff_for(&self, host: String) -> Duration {
        if let Some(h) = self.host.get(host.as_str()) {
            return h.delay_for;
        }

        Duration::from_secs(0)
    }

    pub fn set_backoff(&mut self, host: String, err: FederationError) {
        let mut is_down = false;

        match err {
            FederationError::Connection(e) => {
                is_down = true;
            }
            FederationError::Transaction(e) => {
                if e.error_code == "M_FORBIDDEN" && e.error.starts_with("Federation denied with") {
                    is_down = true;
                } else if e.code.as_u16() == 404 || e.code.as_u16() == 500 {
                    is_down = true;
                }
            }
        }

        if let Some(h) = self.host.get_mut(host.as_str()) {
            h.is_down = is_down;
            h.ts = SystemTime::now()
                .duration_since(SystemTime::UNIX_EPOCH)
                .unwrap()
                .as_millis();

            if !is_down {
                h.delay_for = Duration::from_secs(0);
            }
        } else {
            self.host.insert(
                host,
                SBackoff {
                    delay_for: Duration::from_secs(600),
                    ts: SystemTime::now()
                        .duration_since(SystemTime::UNIX_EPOCH)
                        .unwrap()
                        .as_millis(),
                    is_down,
                },
            );
        }
    }
}

extern crate cortex;

mod backoff;
mod error;
mod federation_client;
mod federation_sender;
mod signing_key;
mod transaction;
mod transaction_queue;

use std::thread;
use std::time::Duration;

use clap::{App, Arg};
use threadpool::ThreadPool;

use cortex::config::Config;
use cortex::replication::connection::Connection;
use cortex::replication::streams::EventStreamRow;

fn main() {
    let matches = App::new("Cortex")
        .version("0.0.0")
        .author("Black Hat <bhat@encom.eu.org>")
        .about("A federation sender worker")
        .arg(
            Arg::with_name("config")
                .short("c")
                .long("config")
                .value_name("FILE")
                .help("Path to config file")
                .default_value("appsettings.yaml")
                .takes_value(true),
        )
        .get_matches();

    let config_path = matches.value_of("config").unwrap();
    let config = Config::from_file(config_path).unwrap();

    let mut conn = Connection::connect(
        config.name + "-federation-sender",
        config.synapse.host,
        config.synapse.port,
    );
    println!("Connection started");

    let mut event_stream = conn.register_stream::<EventStreamRow>();
    event_stream.register_on_data_row(|row| {
        thread::spawn(move || {
            println!("Event stream got row: {}", row);
        });
    });
    event_stream.register_on_position(|token| {
        thread::spawn(move || {
            println!("Event stream got position: {}", token);
        });
    });

    let pool = ThreadPool::new(16);

    conn.sync();
}

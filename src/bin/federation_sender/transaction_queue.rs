use crate::backoff::Backoff;
use crate::federation_client::FederationClient;
use crate::signing_key::SigningKey;
use crate::transaction::Transaction;
use cortex::replication::presence_state::PresenceState;
use std::collections::HashMap;

pub struct TransactionQueue {
    backoff: Backoff,
    client: FederationClient,
    database_url: String,
    dest_last_device_list_stream_id: HashMap<String, u128>,
    dest_last_device_msg_stream_id: HashMap<String, u128>,
    dest_pending_transactions: HashMap<String, Vec<Transaction>>,
    server_name: String,
    user_presence: HashMap<String, PresenceState>,
    last_event_poke: u32,
    signing_key: SigningKey,
    txn_id: u64,
}

impl TransactionQueue {
    const MAX_PDUS_PER_TRANSACTION: u32 = 50;
    const MAX_EDUS_PER_TRANSACTION: u32 = 100;

    //    pub fn new(server_name: String, database_url: String, signing_key: SigningKey) -> Self {}
}

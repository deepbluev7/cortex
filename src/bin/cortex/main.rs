extern crate cortex;

use clap::{App, Arg};

use cortex::config::Config;
use cortex::replication::connection::Connection;
use cortex::replication::streams::EventStreamRow;
use std::thread;

fn main() {
    let matches = App::new("Cortex")
        .version("0.0.0")
        .author("Black Hat <bhat@encom.eu.org>")
        .about("A dev worker")
        .arg(
            Arg::with_name("config")
                .short("c")
                .long("config")
                .value_name("FILE")
                .help("Path to config file")
                .default_value("appsettings.yaml")
                .takes_value(true),
        )
        .get_matches();

    let config_path = matches.value_of("config").unwrap();
    let config = Config::from_file(config_path).unwrap();

    let mut conn = Connection::connect(config.name, config.synapse.host, config.synapse.port);
    println!("Connection started");

    let mut event_stream = conn.register_stream::<EventStreamRow>();
    event_stream.register_on_data_row(|row| {
        thread::spawn(move || {
            println!("Event stream got row: {}", row);
        });
    });
    event_stream.register_on_position(|token| {
        thread::spawn(move || {
            println!("Event stream got position: {}", token);
        });
    });
    conn.sync();
}

use std::collections::HashMap;
use std::fmt;
use std::net::{IpAddr, Ipv4Addr, Ipv6Addr};
use std::str::FromStr;
use std::time::{Duration, SystemTime};

use http::Uri;
use reqwest::Client;
use serde_json::Value;
use std::io;
use trust_dns_resolver::config::*;
use trust_dns_resolver::proto::rr::rdata::SRV;
use trust_dns_resolver::Resolver;

pub struct HostResolver {
    hosts: HashMap<String, HostRecord>,
    default_port: u16,
    resolver: Resolver,
    http_client: Client,
}

impl HostResolver {
    pub fn new(default_port: u16) -> Self {
        HostResolver {
            default_port,
            hosts: HashMap::new(),
            resolver: Resolver::new(ResolverConfig::default(), ResolverOpts::default()).unwrap(),
            http_client: reqwest::Client::new(),
        }
    }

    pub fn host_record(&mut self, destination: String) -> HostRecord {
        // destination might be an IP address or DNS style hostname.
        if let Some(cached_record) = self.hosts.get(destination.as_str()) {
            if cached_record.expired() {
                self.hosts.remove(destination.as_str());
            } else {
                return cached_record.clone();
            }
        }

        let (url, srv_records) = self.resolve(destination.clone());
        let record = HostRecord::new(url, srv_records, destination.clone());
        self.hosts.insert(destination.clone(), record.clone());

        record
    }

    fn resolve(&self, destination: String) -> (http::uri::Uri, Vec<SRV>) {
        if let Ok(url) = self.handle_basic_host(destination.clone()) {
            (url, Vec::new())
        } else {
            let mut raw_url = self.create_host_url(destination.clone());

            // Resolve .well-known
            if let Ok(mut resp) = self
                .http_client
                .get(format!("https://{}/.well-known/matrix/server", destination.clone()).as_str())
                .send()
            {
                if resp.status().is_success() {
                    let body = resp.text().unwrap();
                    let v: Value = serde_json::from_str(body.as_str()).unwrap();
                    if let Some(well_known_host_value) = v.get("m.server") {
                        let well_known_host = well_known_host_value.as_str().unwrap().to_string();
                        if let Ok(url) = self.handle_basic_host(well_known_host) {
                            return (url, Vec::new());
                        }
                        raw_url = self.create_host_url(destination.clone());
                    }
                }
            }

            // Resolve SRV
            if let Ok(response) = self
                .resolver
                .lookup_srv(format!("_matrix._tcp.{}.", raw_url.host().unwrap()).as_str())
            {
                let srv_records: Vec<SRV> = response.iter().map(|s| s.clone()).collect();

                if srv_records.len() > 0 {
                    let srv_record = srv_records.get(0).unwrap();
                    let mut target = srv_record.target().to_string();
                    if srv_record.target().is_fqdn() {
                        target.pop();
                    }
                    let real_url =
                        self.create_host_url(format!("{}:{}", target, srv_record.port()));
                    return (real_url, srv_records);
                }
            }
            (raw_url, Vec::new())
        }
    }

    fn handle_basic_host(&self, destination: String) -> Result<http::uri::Uri, io::Error> {
        let raw_url = self.create_host_url(destination.clone());

        if let Ok(_) = destination.parse::<IpAddr>() {
            Ok(raw_url)
        } else if http::uri::Uri::from_str(format!("https://{}", destination).as_str())
            .unwrap()
            .port_u16()
            .is_some()
        {
            Ok(raw_url)
        } else {
            Err(io::Error::new(io::ErrorKind::InvalidData, ""))
        }
    }

    fn create_host_url(&self, destination: String) -> http::uri::Uri {
        let unprocessed_uri =
            http::uri::Uri::from_str(format!("https://{}", destination).as_str()).unwrap();
        if unprocessed_uri.port_u16().is_none() {
            http::uri::Uri::from_str(
                format!(
                    "https://{}:{}",
                    unprocessed_uri.authority_part().unwrap(),
                    self.default_port
                )
                .as_str(),
            )
            .unwrap()
        } else {
            unprocessed_uri
        }
    }
}

#[derive(Clone, Debug)]
pub enum HostStatus {
    UNKNOWN,
    DOWN,
    UP,
}

#[derive(Clone, Debug)]
pub struct HostRecord {
    pub host: String,
    pub resolved_url: Uri,
    pub entries: Vec<SRV>,
    pub status: HostStatus,
    pub last_access_time: u128,
}

impl HostRecord {
    const TTL: Duration = Duration::from_secs(3600);

    pub fn new(url: Uri, entries: Vec<SRV>, host: String) -> Self {
        HostRecord {
            host,
            resolved_url: url,
            entries,
            status: HostStatus::UNKNOWN,
            last_access_time: SystemTime::now()
                .duration_since(SystemTime::UNIX_EPOCH)
                .unwrap()
                .as_millis(),
        }
    }

    pub fn expired(&self) -> bool {
        SystemTime::now()
            .duration_since(SystemTime::UNIX_EPOCH)
            .unwrap()
            .as_millis()
            > self.last_access_time + Self::TTL.as_millis()
    }
}

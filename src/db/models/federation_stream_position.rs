#[derive(Debug, Queryable)]
pub struct FederationStreamPosition {
    pub name: String,
    pub stream_id: i32,
}

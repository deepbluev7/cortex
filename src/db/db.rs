use diesel::pg::PgConnection;
use diesel::prelude::*;

pub fn establish_connection(url: &str) -> PgConnection {
    PgConnection::establish(url).expect(&format!("Error connecting to {}", url))
}

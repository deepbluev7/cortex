table! {
    federation_stream_position (name) {
        #[sql_name = "type"]
        name -> Text,
        stream_id -> Integer,
    }
}

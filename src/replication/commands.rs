use std::fmt;

pub trait Command: fmt::Display {
    const NAME: &'static str;

    fn from_line(line: String) -> Self;
    fn to_line(&self) -> String;
}

pub struct ServerCommand {
    name: String,
}

impl Command for ServerCommand {
    const NAME: &'static str = "SERVER";

    fn from_line(line: String) -> Self {
        ServerCommand { name: line }
    }

    fn to_line(&self) -> String {
        self.name.clone()
    }
}

impl fmt::Display for ServerCommand {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{} {}", Self::NAME, self.to_line())
    }
}

pub struct RDataCommand {
    pub stream: String,
    pub token: u128,
    pub row_data: String,
}

impl RDataCommand {
    pub fn is_batched(&self) -> bool {
        self.token == 0
    }
}

impl Command for RDataCommand {
    const NAME: &'static str = "RDATA";

    fn from_line(line: String) -> Self {
        let mut iter = line.splitn(3, char::is_whitespace);
        let stream = iter.next().unwrap().to_string();
        let token = iter.next().unwrap().parse::<u128>().unwrap_or(0);
        let row_data = iter.next().unwrap().to_string();

        RDataCommand {
            stream,
            token,
            row_data,
        }
    }

    fn to_line(&self) -> String {
        if self.token == 0 {
            format!("{} batch {}", self.stream, self.row_data)
        } else {
            format!("{} {} {}", self.stream, self.token, self.row_data)
        }
    }
}

impl fmt::Display for RDataCommand {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{} {}", Self::NAME, self.to_line())
    }
}

pub struct PositionCommand {
    pub stream: String,
    pub token: u128,
}

impl Command for PositionCommand {
    const NAME: &'static str = "POSITION";

    fn from_line(line: String) -> Self {
        let mut iter = line.splitn(2, char::is_whitespace);
        let stream = iter.next().unwrap().to_string();
        let token = iter.next().unwrap().parse::<u128>().unwrap();

        PositionCommand { stream, token }
    }

    fn to_line(&self) -> String {
        format!("{} {}", self.stream, self.token)
    }
}

impl fmt::Display for PositionCommand {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{} {}", Self::NAME, self.to_line())
    }
}

pub struct ErrorCommand {}

impl Command for ErrorCommand {
    const NAME: &'static str = "ERROR";

    fn from_line(line: String) -> Self {
        ErrorCommand {}
    }

    fn to_line(&self) -> String {
        String::new()
    }
}

impl fmt::Display for ErrorCommand {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{} {}", Self::NAME, self.to_line())
    }
}

pub struct PingCommand {
    pub millis_since_epoch: u128,
}

impl Command for PingCommand {
    const NAME: &'static str = "PING";

    fn from_line(line: String) -> Self {
        PingCommand {
            millis_since_epoch: line.parse::<u128>().unwrap(),
        }
    }

    fn to_line(&self) -> String {
        self.millis_since_epoch.to_string()
    }
}

impl fmt::Display for PingCommand {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{} {}", Self::NAME, self.to_line())
    }
}

pub struct NameCommand {
    pub name: String,
}

impl Command for NameCommand {
    const NAME: &'static str = "NAME";

    fn from_line(line: String) -> Self {
        NameCommand { name: line }
    }

    fn to_line(&self) -> String {
        self.name.clone()
    }
}

impl fmt::Display for NameCommand {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{} {}", Self::NAME, self.to_line())
    }
}

pub struct ReplicateCommand {
    pub stream: String,
    pub token: u128,
}

impl Command for ReplicateCommand {
    const NAME: &'static str = "REPLICATE";

    fn from_line(line: String) -> Self {
        let mut iter = line.splitn(2, char::is_whitespace);
        let stream = iter.next().unwrap().to_string();
        let token = iter.next().unwrap().parse::<u128>().unwrap();

        ReplicateCommand { stream, token }
    }

    fn to_line(&self) -> String {
        if self.token == 0 {
            format!("{} NOW", self.stream)
        } else {
            format!("{} {}", self.stream, self.token)
        }
    }
}

impl fmt::Display for ReplicateCommand {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{} {}", Self::NAME, self.to_line())
    }
}

pub struct UserSyncCommand {}

impl Command for UserSyncCommand {
    const NAME: &'static str = "USER_SYNC";

    fn from_line(line: String) -> Self {
        UserSyncCommand {}
    }

    fn to_line(&self) -> String {
        String::new()
    }
}

impl fmt::Display for UserSyncCommand {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{} {}", Self::NAME, self.to_line())
    }
}

pub struct FederationAckCommand {}

impl Command for FederationAckCommand {
    const NAME: &'static str = "FEDERATION_ACK";

    fn from_line(line: String) -> Self {
        FederationAckCommand {}
    }

    fn to_line(&self) -> String {
        String::new()
    }
}

impl fmt::Display for FederationAckCommand {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{} {}", Self::NAME, self.to_line())
    }
}

pub struct SyncCommand {}

impl Command for SyncCommand {
    const NAME: &'static str = "SYNC";

    fn from_line(line: String) -> Self {
        SyncCommand {}
    }

    fn to_line(&self) -> String {
        String::new()
    }
}

impl fmt::Display for SyncCommand {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{} {}", Self::NAME, self.to_line())
    }
}

pub struct RemovePusherCommand {}

impl Command for RemovePusherCommand {
    const NAME: &'static str = "REMOVE_PUSHER";

    fn from_line(line: String) -> Self {
        RemovePusherCommand {}
    }

    fn to_line(&self) -> String {
        String::new()
    }
}

impl fmt::Display for RemovePusherCommand {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{} {}", Self::NAME, self.to_line())
    }
}

pub struct InvalidateCacheCommand {}

impl Command for InvalidateCacheCommand {
    const NAME: &'static str = "INVALIDATE_CACHE";

    fn from_line(line: String) -> Self {
        InvalidateCacheCommand {}
    }

    fn to_line(&self) -> String {
        String::new()
    }
}

impl fmt::Display for InvalidateCacheCommand {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{} {}", Self::NAME, self.to_line())
    }
}

pub struct UserIpCommand {}

impl Command for UserIpCommand {
    const NAME: &'static str = "USER_IP";

    fn from_line(line: String) -> Self {
        UserIpCommand {}
    }

    fn to_line(&self) -> String {
        String::new()
    }
}

impl fmt::Display for UserIpCommand {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{} {}", Self::NAME, self.to_line())
    }
}

pub enum CommandMap {
    Server(ServerCommand),
    RData(RDataCommand),
    Position(PositionCommand),
    Error(ErrorCommand),
    Ping(PingCommand),
    Name(NameCommand),
    Replicate(ReplicateCommand),
    UserSync(UserSyncCommand),
    FederationAck(FederationAckCommand),
    Sync(SyncCommand),
    RemovePusher(RemovePusherCommand),
    InvalidateCache(InvalidateCacheCommand),
    UserIp(UserIpCommand),
}

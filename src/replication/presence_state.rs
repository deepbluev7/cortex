use serde::Deserialize;

#[derive(Debug, Deserialize)]
pub struct PresenceState {
    pub state: String,
    pub user_id: String,
    pub last_user_sync_ts: u64,
    pub last_active_ts: u64,
    pub last_federation_update_ts: u64,
    pub status_msg: String,
    pub currently_active: bool,
}

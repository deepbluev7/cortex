use crate::replication::edu_event::EduEvent;
use crate::replication::presence_state::PresenceState;
use serde_json::Value;
use std::collections::HashMap;
use std::fmt;
use typemap::Key;

pub struct Stream<T: StreamRow> {
    data_row: Vec<T>,
    token: u128,

    on_data_row: fn(T),
    on_position: fn(u128),
}

impl<T: StreamRow + 'static> Stream<T> {
    pub fn new(token: u128) -> Stream<T> {
        Stream {
            data_row: vec![],
            token,
            on_data_row: |_| {},
            on_position: |_| {},
        }
    }

    pub fn stream_name() -> &'static str {
        T::NAME
    }

    pub fn register_on_data_row(&mut self, f: fn(T)) {
        self.on_data_row = f;
    }

    pub fn register_on_position(&mut self, f: fn(u128)) {
        self.on_position = f;
    }

    pub fn process_position(&mut self, token: u128) {
        if self.token != token {
            self.token = token;
            (self.on_position)(token);
        }
    }

    pub fn process_rdata(&mut self, token: u128, data_rows: Vec<String>) {
        self.process_position(token);

        for data_row in data_rows {
            (self.on_data_row)(T::from_raw(data_row));
        }
    }
}

impl<T: StreamRow + 'static> Key for Stream<T> {
    type Value = Stream<T>;
}

pub trait StreamRow: fmt::Display {
    const NAME: &'static str;

    fn from_raw(str: String) -> Self;
}

pub struct EventStreamRow {
    event_id: String,
    room_id: String,
    event_type: String,
    state_key: Option<String>,
    redacts: Option<String>,
}

impl StreamRow for EventStreamRow {
    const NAME: &'static str = "events";

    fn from_raw(data: String) -> EventStreamRow {
        let v: Value = serde_json::from_str(data.as_str()).unwrap();

        EventStreamRow {
            event_id: v[0].to_string(),
            room_id: v[1].to_string(),
            event_type: v[2].to_string(),
            state_key: v.get(3).map(|s| s.to_string()),
            redacts: v.get(4).map(|s| s.to_string()),
        }
    }
}

impl fmt::Display for EventStreamRow {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{} {} {} {} {}",
            self.event_id,
            self.room_id,
            self.event_type,
            self.state_key.as_ref().unwrap(),
            self.redacts.as_ref().unwrap()
        )
    }
}

pub struct FederationStreamRow {
    devices: Vec<String>,
    edus: Vec<EduEvent>,
    keyed_edus: HashMap<Vec<String>, EduEvent>,
    presence: Vec<PresenceState>,
}

impl StreamRow for FederationStreamRow {
    const NAME: &'static str = "federation";

    fn from_raw(data: String) -> FederationStreamRow {
        let v: Value = serde_json::from_str(data.as_str()).unwrap();
        let arr = v.as_array().unwrap();
        let mut type_id = "";

        let mut row = FederationStreamRow {
            devices: vec![],
            edus: vec![],
            keyed_edus: HashMap::new(),
            presence: vec![],
        };

        for i in 0..arr.len() {
            if i == 0 {
                type_id = arr[0].as_str().unwrap();
                continue;
            }
            let parsed = arr[i].clone();
            match type_id {
                "k" => {
                    let edu = EduEvent {
                        edu_type: parsed["edu"]["edu_type"].to_string(),
                        internal_key: "".to_string(),
                        stream_id: 0,
                        content: parsed["edu"]["content"].to_string(),
                        origin: parsed["edu"]["origin"].to_string(),
                        destination: parsed["edu"]["destination"].to_string(),
                    };
                    let key = parsed["key"]
                        .as_array()
                        .unwrap()
                        .clone()
                        .iter()
                        .map(|s| s.clone().as_str().unwrap().to_string())
                        .collect();
                    row.keyed_edus.insert(key, edu);
                }
                "p" => {
                    let presence_state = serde_json::from_value(parsed).unwrap();
                    row.presence.push(presence_state);
                }
                "e" => row.edus.push(EduEvent {
                    content: parsed["content"].to_string(),
                    edu_type: parsed["edu_type"].to_string(),
                    internal_key: "".to_string(),
                    stream_id: 0,
                    origin: parsed["origin"].to_string(),
                    destination: parsed["destination"].to_string(),
                }),
                "d" => {
                    let destination = parsed["destination"].to_string();
                    row.devices.push(destination);
                }
                (_) => {}
            }
        }

        row
    }
}

impl fmt::Display for FederationStreamRow {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "",)
    }
}

pub enum StreamRowMap {}

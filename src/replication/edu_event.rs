use serde::Deserialize;

#[derive(Debug, Deserialize)]
pub struct EduEvent {
    pub content: String,
    pub edu_type: String,
    pub internal_key: String,
    pub stream_id: u64,
    pub origin: String,
    pub destination: String,
}

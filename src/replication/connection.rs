use std::io;
use std::io::prelude::*;
use std::io::BufReader;
use std::net::TcpStream;
use std::time::{Duration, SystemTime, UNIX_EPOCH};

use typemap::TypeMap;

use crate::replication::commands::CommandMap::{Name, Ping, Position, RData, Replicate, Server};
use crate::replication::commands::{
    Command, CommandMap, NameCommand, PingCommand, PositionCommand, RDataCommand, ReplicateCommand,
    ServerCommand,
};
use crate::replication::streams::StreamRow;
use crate::replication::streams::{EventStreamRow, FederationStreamRow, Stream};
use crate::util::timer::{Guard, Timer};
use std::collections::HashMap;

pub struct Connection {
    name: String,

    host: String,
    port: u16,

    stream: TcpStream,
    timer: Timer,
    ping_guard: Guard,
    buf_reader: BufReader<TcpStream>,

    pending_batches: HashMap<String, Vec<String>>,

    registered_streams: TypeMap,
}

impl Connection {
    pub fn connect(name: String, host: String, port: u16) -> Connection {
        let replication_addr = format!("{}:{}", host, port);
        let mut stream = TcpStream::connect(replication_addr).unwrap();

        stream.write_line(format!("{}", NameCommand { name: name.clone() }));

        let mut ping_stream = stream.try_clone().unwrap();
        let timer = Timer::new();
        let ping_guard = timer.schedule_repeating(Duration::from_secs(5), move || {
            let seconds_since_epoch = SystemTime::now()
                .duration_since(UNIX_EPOCH)
                .expect("Time went backwards");

            ping_stream.write_line(format!(
                "{}",
                PingCommand {
                    millis_since_epoch: seconds_since_epoch.as_millis()
                }
            ));
        });

        let read_stream = stream.try_clone().unwrap();
        let buf_reader = BufReader::new(read_stream);

        Connection {
            host,
            port,
            name,
            stream,
            timer,
            ping_guard,
            buf_reader,
            pending_batches: HashMap::new(),
            registered_streams: TypeMap::new(),
        }
    }

    pub fn write_line(&mut self, line: String) -> io::Result<()> {
        self.stream.write_line(line)
    }

    pub fn sync(&mut self) -> io::Result<()> {
        let iter = self.buf_reader.by_ref().lines().map(Self::process_line);
        for c in iter {
            let command = c?;
            match command {
                Server(server_command) => println!("{}", server_command),
                Name(name_command) => println!("{}", name_command),
                Ping(ping_command) => {}
                RData(rdata_command) => {
                    println!("{}", rdata_command);
                    let stream_name = rdata_command.stream.clone();
                    let has_pending = self.pending_batches.contains_key(stream_name.as_str());
                    let is_batched = rdata_command.is_batched();

                    if is_batched {
                        if !has_pending {
                            self.pending_batches.insert(stream_name.clone(), Vec::new());
                        }
                        self.pending_batches
                            .get_mut(stream_name.as_str())
                            .unwrap()
                            .push(rdata_command.row_data);
                        continue;
                    } else {
                        if has_pending {
                            let mut pending_batches =
                                self.pending_batches.remove(stream_name.as_str()).unwrap();
                            pending_batches.push(rdata_command.row_data);

                            match rdata_command.stream.as_str() {
                                (EventStreamRow::NAME) => self
                                    .registered_streams
                                    .get_mut::<Stream<EventStreamRow>>()
                                    .unwrap()
                                    .process_rdata(rdata_command.token, pending_batches),
                                (FederationStreamRow::NAME) => self
                                    .registered_streams
                                    .get_mut::<Stream<FederationStreamRow>>()
                                    .unwrap()
                                    .process_rdata(rdata_command.token, pending_batches),
                                (_) => {}
                            }
                            continue;
                        }
                        match rdata_command.stream.as_str() {
                            (EventStreamRow::NAME) => self
                                .registered_streams
                                .get_mut::<Stream<EventStreamRow>>()
                                .unwrap()
                                .process_rdata(rdata_command.token, vec![rdata_command.row_data]),
                            (FederationStreamRow::NAME) => self
                                .registered_streams
                                .get_mut::<Stream<FederationStreamRow>>()
                                .unwrap()
                                .process_rdata(rdata_command.token, vec![rdata_command.row_data]),
                            (_) => {}
                        }
                        continue;
                    }
                }
                Position(position_command) => {
                    println!("{}", position_command);
                    match position_command.stream.as_str() {
                        (EventStreamRow::NAME) => self
                            .registered_streams
                            .get_mut::<Stream<EventStreamRow>>()
                            .unwrap()
                            .process_position(position_command.token),
                        (FederationStreamRow::NAME) => self
                            .registered_streams
                            .get_mut::<Stream<FederationStreamRow>>()
                            .unwrap()
                            .process_position(position_command.token),
                        (_) => {}
                    }
                }
                _ => {}
            }
        }
        Ok(())
    }

    pub fn register_stream<T: StreamRow + 'static>(&mut self) -> &mut Stream<T> {
        let position = 0; // which is equal to "NOW"

        self.subscribe_stream(T::NAME.to_string(), position);
        self.registered_streams
            .insert::<Stream<T>>(Stream::new(position));
        self.registered_streams.get_mut::<Stream<T>>().unwrap()
    }

    fn subscribe_stream(&mut self, stream: String, token: u128) -> io::Result<()> {
        self.write_line(ReplicateCommand { stream, token }.to_string())
    }

    fn process_line(res: Result<String, io::Error>) -> Result<CommandMap, io::Error> {
        res.and_then(|line| {
            let mut command_iter = line.splitn(2, char::is_whitespace);
            match command_iter.next() {
                Some(command_name) => {
                    let args = command_iter.next().unwrap().to_string();
                    match command_name {
                        (NameCommand::NAME) => Ok(CommandMap::Name(NameCommand {
                            name: "".to_string(),
                        })),
                        (ServerCommand::NAME) => {
                            Ok(CommandMap::Server(ServerCommand::from_line(args)))
                        }
                        (PingCommand::NAME) => Ok(CommandMap::Ping(PingCommand::from_line(args))),
                        (PositionCommand::NAME) => {
                            Ok(CommandMap::Position(PositionCommand::from_line(args)))
                        }
                        (RDataCommand::NAME) => {
                            Ok(CommandMap::RData(RDataCommand::from_line(args)))
                        }
                        _ => Err(io::Error::new(
                            io::ErrorKind::InvalidData,
                            "invalid command",
                        )),
                    }
                }
                None => Err(io::Error::new(
                    io::ErrorKind::InvalidData,
                    "invalid command",
                )),
            }
        })
    }
}

trait LineWriter {
    fn write_line(&mut self, line: String) -> io::Result<()>;
}

impl LineWriter for TcpStream {
    fn write_line(&mut self, line: String) -> io::Result<()> {
        self.write_all((line + "\n").as_bytes())
    }
}

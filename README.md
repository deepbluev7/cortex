# Cortex

A group of workers for offloading your Synapse pressure.

# Building

```bash
cargo build --bin cortex
```

# Running

```bash
cortex --config appsettings.yaml
```
